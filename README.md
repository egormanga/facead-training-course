# facead-training-course
Краткий (нет) вступительный курс команды FaceAd. Охватывает базовые функции Git, Bitbucket, консольные утилиты Linux, язык Python 3 и его библиотеки Flask, Pillow и OpenCV, а также приучает к нашим строгим гайдлайнам (см. GUIDELINES.md).
